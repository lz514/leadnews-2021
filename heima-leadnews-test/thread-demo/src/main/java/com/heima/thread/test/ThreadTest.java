package com.heima.thread.test;

import java.util.concurrent.Callable;

public class ThreadTest {

    public static void main(String[] args) {

        myThread myThread = new myThread();
        myThread.setName("myThread-线程");
        myThread.start();



    }

    /**
     * 创建线程第一种方式
     */
    public  static class myThread extends Thread{

        public void run() {
            System.out.println(Thread.currentThread().getName());
        }
    }


    /**
     * 创建线程第二种方式
     */
    public  static class myRunnable implements Runnable{

        public void run() {
            System.out.println(Thread.currentThread().getName());
        }
    }

    /**
     * 创建线程第三种方法
     */
    public  static class mycallable implements Callable {

        public String call() {
            System.out.println(Thread.currentThread().getName());

            return "";
        }
    }
}
