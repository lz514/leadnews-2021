package com.xuecheng.test.freemarker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Freemarkerspringapp {
    public static void main(String[] args) {
        SpringApplication.run(Freemarkerspringapp.class);
    }
}
