package com.xuecheng.test.freemarker.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/basic")
public class HelloController {

    @GetMapping("/v1")
    public String view(ModelMap map) {
        map.addAttribute("name", "zhangkai");
        return "01-basic";
    }
}
