package com.heima;

import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class MinioTest {
    public static void main(String[] args) throws IOException, ServerException, InvalidBucketNameException, InsufficientDataException, ErrorResponseException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        //获取上传文件资源
        FileInputStream inputStream = new FileInputStream("C:\\Users\\asus\\Desktop\\桌面壁纸\\wallhaven-3z8qjd.png");

        //1.创建minioclieent客户端
        MinioClient build = MinioClient.builder()
                .credentials("minio", "minio123")
                .endpoint("http://121.40.32.194:9000")
                .build();

        //上传文件
        PutObjectArgs leadnews = PutObjectArgs.builder()
                .object("aa.png")
                .contentType("jpg/png")
                .bucket("leadnews")
                .stream(inputStream,inputStream.available(),-1)
                .build();

        //上传
        build.putObject(leadnews);

        System.out.println("http://121.40.32.194:9000/leadnews/aa.png");
    }
}
