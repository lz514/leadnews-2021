package com.heima.app.gateway.filter;

import com.heima.app.gateway.jwt.AppJwtUtil;
import io.jsonwebtoken.Claims;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class AuthorizeFilter implements Ordered, GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1.获取request和response对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        //2.判断是否是登录
        if (request.getURI().getPath().contains("/login")) {
            //放行
            return chain.filter(exchange);
        }
        //3.不是登录需要验证
        //获取tocken
        String token = request.getHeaders().getFirst("token");
        //判断是否为空
        if (StringUtil.isNullOrEmpty(token)) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();//不通过，拒绝
        }

        //不为空判断是否过期


        try {
            Claims claimsBody = AppJwtUtil.getClaimsBody(token);
            int i = AppJwtUtil.verifyToken(claimsBody);
            if (i==1 ||i==2) {
                //说明token失效
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();//不通过，拒绝

            }
        } catch (Exception e) {
            e.printStackTrace();//打印出异常，并且输出在哪里出现的异常
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();//不通过，拒绝
        }

        //没过期放行
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
