package com.heima.user.service.Imp;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.user.pojos.ApUser;
import com.heima.model.common.user.pojos.LoginDto;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserService;
import com.heima.utils.common.AppJwtUtil;
import com.mysql.jdbc.StringUtils;
import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.HashMap;

@Service
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {

    /**
     * 登录
     * @param loginDto
     * @return
     */
    public ResponseResult login(LoginDto loginDto) {
        HashMap<String, Object> map = new HashMap<>();
        //正常登录（手机号+密码）
        if (StringUtil.isBlank(loginDto.getPhone()) && StringUtil.isBlank(loginDto.getPassword())) {

            //没有账号密码证明是游客登录则游客id设置为0
            map.put("token", AppJwtUtil.getToken(0l));
            return ResponseResult.okResult(map);

        }
        //不为空则表示有账号密码登录
        //根据手机号查询用户
        ApUser one = getOne(Wrappers.<ApUser> lambdaQuery().eq(ApUser::getPhone, loginDto.getPhone()));
        if (one==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "用户不存在");
        }
        //不为空则验证密码
        String salt = one.getSalt();
        String password = loginDto.getPassword();
        password = DigestUtils.md5DigestAsHex((password + salt).getBytes());
        System.out.println(password);
        //比对
        if (password.equals(one.getPassword())) {
            map.put("token", AppJwtUtil.getToken(one.getId().longValue()));
            //成功则返回
            one.setSalt("");
            one.setPassword("");
            map.put("user", one);
            return ResponseResult.okResult(map);

        }
        //密码不对

        return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
    }

}
