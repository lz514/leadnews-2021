package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.user.pojos.ApUser;
import com.heima.model.common.user.pojos.LoginDto;

public interface ApUserService extends IService<ApUser> {
    public ResponseResult login(LoginDto loginDto);
}
