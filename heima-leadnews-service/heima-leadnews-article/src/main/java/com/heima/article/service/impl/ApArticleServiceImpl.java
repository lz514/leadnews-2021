package com.heima.article.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.ApArticleService;
import com.heima.article.service.ArticleFreemarkerService;
import com.heima.model.common.article.ArticleConstants;
import com.heima.model.common.article.dtos.ArticleDto;
import com.heima.model.common.article.dtos.ArticleHomeDto;
import com.heima.model.common.article.pojos.ApArticle;
import com.heima.model.common.article.pojos.ApArticleConfig;
import com.heima.model.common.article.pojos.ApArticleContent;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class ApArticleServiceImpl extends ServiceImpl<ApArticleMapper,ApArticle>  implements ApArticleService {

    //单页最大加载的数字
    private final static Short MAX_PAGE_SIZE = 50;

    //发布文章存放
    @Autowired
    private ApArticleMapper apArticleMapper;

    //文章配置mapper
    @Autowired
    private ApArticleConfigMapper apArticleConfigMapper;

    //文章内容
    @Autowired
    private ApArticleContentMapper apArticleContentMapper;

    //文章文件发布
    @Autowired
    ArticleFreemarkerService articleFreemarkerService;

    /**
     * 根据参数加载文章列表
     * @param loadtype 1.为加载更多  2.为加载最新
     * @param dto
     * @return
     */
    public ResponseResult load(Short loadtype, ArticleHomeDto dto) {
        //校验参数
        Integer size = dto.getSize();
        if (size==null || size==0) {
            size = 10;
        }

        size = Math.min(size,MAX_PAGE_SIZE); //返回其中最小的值
        dto.setSize(size);

        //参数类型校验 不是1,也不是all则是2
        if (!loadtype.equals(ArticleConstants.LOADTYPE_LOAD_MORE)&&!loadtype.equals(ArticleConstants.DEFAULT_TAG)) {
            loadtype = ArticleConstants.LOADTYPE_LOAD_NEW;
        }

        //文章频道校验
        if (!StringUtil.isNullOrEmpty(dto.getTag())) {
            dto.setTag(ArticleConstants.DEFAULT_TAG);
        }

        //时间校验
        if (dto.getMinBehotTime()==null) dto.setMinBehotTime(new Date());
        if (dto.getMaxBehotTime()==null) dto.setMaxBehotTime(new Date());

        //查询数据
       List<ApArticle> apArticles= apArticleMapper.loadArticleList(dto, loadtype);

       //封装结果集
        ResponseResult responseResult = ResponseResult.okResult(apArticles);
        return responseResult;
    }


    /**
     * 文章通过后保存
     * @param dto
     * @return
     */
    @Override
    public ResponseResult saveArticle(ArticleDto dto) {
        //判断参数
        if (dto==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //创建文章对象
        ApArticle apArticle = null;

        //判断师傅有id
        if (dto.getId() == null) {
            apArticle = new ApArticle();
            //属性转义
            BeanUtils.copyProperties(dto, apArticle);
            //1.保存文章信息
            apArticleMapper.insert(apArticle);
            //2.保存文章配置信息
            ApArticleConfig apArticleConfig = new ApArticleConfig(apArticle.getId());
            apArticleConfigMapper.insert(apArticleConfig);
            //3.保存文章内容信息
            ApArticleContent apArticleContent = new ApArticleContent();
            apArticleContent.setArticleId(dto.getId());
            apArticleContent.setContent(dto.getContent());
            apArticleContentMapper.insert(apArticleContent);

        } else {//修改文章加内容
            //先根据id查询再修改
            apArticle=apArticleMapper.selectById(dto.getId());
            BeanUtils.copyProperties(dto, apArticle);
            apArticleMapper.updateById(apArticle);
            //查询文章内容并修改
            ApArticleContent apArticleContent = apArticleContentMapper
                    .selectOne(Wrappers.<ApArticleContent>lambdaQuery()
                            .eq(ApArticleContent::getArticleId, apArticle.getId()));
            apArticleContent.setContent(dto.getContent());
            apArticleContentMapper.updateById(apArticleContent);

        }
       //异步调用 生成静态文件上传到minio中
        articleFreemarkerService.buildArticleToMinIO(apArticle,dto.getContent());

        return ResponseResult.okResult(apArticle == null ? "" : apArticle.getId());
    }

}
