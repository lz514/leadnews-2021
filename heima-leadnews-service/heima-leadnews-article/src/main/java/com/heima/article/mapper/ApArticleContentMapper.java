package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.article.pojos.ApArticleContent;



public interface ApArticleContentMapper extends BaseMapper<ApArticleContent> {
}
