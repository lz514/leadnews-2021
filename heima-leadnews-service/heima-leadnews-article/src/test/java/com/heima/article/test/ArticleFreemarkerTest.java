package com.heima.article.test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.nacos.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.file.service.FileStorageService;
import com.heima.model.common.article.pojos.ApArticle;
import com.heima.model.common.article.pojos.ApArticleContent;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ArticleFreemarkerTest {

    @Autowired
    ApArticleContentMapper apArticleContentMapper;

    @Autowired
    FileStorageService fileStorageService;


    @Autowired
    private Configuration configuration;

    @Autowired
    ApArticleMapper apArticleMapper;

    @Test
    public  void createStaticUrlTest() throws IOException, TemplateException {
        //1.获取文章内容
        ApArticleContent apArticleContent = apArticleContentMapper.selectOne(Wrappers.<ApArticleContent>lambdaQuery().eq(ApArticleContent::getArticleId, "1302862387124125698"));
        if (apArticleContent!=null&& !StringUtils.isBlank(apArticleContent.getContent())) {
            //2.文章通过freemarker生成html文件
            //创建string流
            StringWriter stringWriter = new StringWriter();
            //获取template模板对象
            Template template = configuration.getTemplate("article.ftl"); //获取模板文件
            //创建map对象，存放数据
            HashMap<String, Object> map = new HashMap<>();
            //存放content数据搭配map
            map.put("content", JSONArray.parseArray(apArticleContent.getContent()));
            //将数据存放在模板中并转换成string字符流
            template.process(map,stringWriter);

            //将stringwriter转成inputstream流
            InputStream byteArrayInputStream = new ByteArrayInputStream(stringWriter.toString().getBytes());


            //3.把html文件上传到minio中
            String path = fileStorageService.uploadHtmlFile("", apArticleContent.getArticleId() + ".html", byteArrayInputStream);


            //4.修改ap_article表，保存static_url字段
            ApArticle apArticle = new ApArticle();
            apArticle.setId(apArticleContent.getArticleId());
            apArticle.setStaticUrl(path);
            apArticleMapper.updateById(apArticle);



        }
        return;

    }


}
