package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.wemedia.dto.WmNewsDto;
import com.heima.model.common.wemedia.dto.WmNewsPageReqDto;
import com.heima.model.common.wemedia.pojos.WmMaterial;
import com.heima.model.common.wemedia.pojos.WmNews;
import com.heima.model.common.wemedia.pojos.WmNewsMaterial;
import com.heima.model.common.wemedia.pojos.WmUser;
import com.heima.utils.common.thread.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.service.WmNewsAutoScanService;
import com.heima.wemedia.service.WmNewsService;
import com.heima.wemedia.service.WmNewsTaskService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WmNewsServiceImpl extends ServiceImpl<WmNewsMapper, WmNews> implements WmNewsService {

    @Autowired
    private WmNewsMaterialMapper wmNewsMaterialMapper;

    @Autowired
    private WmMaterialMapper wmMaterialMapper;

    @Autowired
    private WmNewsAutoScanService wmNewsAutoScanService;

    @Autowired
    private WmNewsTaskService wmNewsTaskService;

    /**
     * 根据条件查询文章数据
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findAll(WmNewsPageReqDto dto) {

        //校验参数
        if (dto==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //分页参数检查
        dto.checkParam();

        //获取分页参数
        Integer page = dto.getPage();
        Integer size = dto.getSize();
        //获取登录人信息
        WmUser user = WmThreadLocalUtil.getUser();
        //判断用户是否为空
        if (user==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.TOKEN_REQUIRE);
        }

        //设置分页条件
        IPage page1 = new Page(page, size);

        //创建条件对象
        LambdaQueryWrapper<WmNews> wrapper = new LambdaQueryWrapper<>();
        //设置查询条件
        //状态查询精确
        if (dto.getStatus()!=null) {

            wrapper.eq(WmNews::getStatus, dto.getStatus());
        }

        //频道精确
        if (dto.getChannelId()!=null) {
            wrapper.eq(WmNews::getChannelId, dto.getChannelId());
        }

        //时间范围查询
        if (dto.getBeginPubdate()!=null && dto.getEndPubdate()!=null) {

            wrapper.between(WmNews::getCreatedTime, dto.getBeginPubdate(), dto.getEndPubdate());
        }

        //关键字模糊查询
        if (dto.getKeyword()!=null) {
            wrapper.like(WmNews::getTitle, dto.getKeyword());
        }

        //当前登录人查询
        wrapper.eq(WmNews::getUserId, user.getId());

        //发布时间倒叙
        wrapper.orderByDesc(WmNews::getCreatedTime);


        //查询
         page1 = page(page1, wrapper);

        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getSize(), (int) page1.getTotal());
        pageResponseResult.setData(page1.getRecords());

        return pageResponseResult;
    }

    /**
     * 文章发布，或修改
     * @param dto
     * @return
     */
    @Override
    public ResponseResult submitNews(WmNewsDto dto) {

        //判断参数是否为空
        if (dto==null ||dto.getContent()==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);

        }


        //判断文章排版，为-1则改为null
        if (dto.getType()==-1) {
            dto.setType(null);
        }

        WmNews wmNews = new WmNews();
        //将对象进行copy
        BeanUtils.copyProperties(dto,wmNews);
        ///将images封装到WmNews对象中
        List<String> images = dto.getImages();
        //利用工具类进行提取
        if (images!=null && images.size()>0) {
            String image = StringUtils.join(images, ",");
            wmNews.setImages(image);
        }

        //抽取文章中的图片
        List<String> potoo = Potocq(wmNews.getContent());

        //将封面图片进行封装
        if (wmNews.getType()==null) {
            //抽取封装方法进行封装
            wmNews = Potosfengzhuang(wmNews, potoo);
        }

        extracted(wmNews, images);
         //审核文章
        //wmNewsAutoScanService.autoScanWmNews(wmNews.getId());
        //替换
        wmNewsTaskService.addNewsToTask(wmNews.getId(),wmNews.getPublishTime());
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 删除文章
     * @param id
     */
    @Override
    public void deleteByid(Long id) {
        //删除文章对应的素材关系表数据
        wmNewsMaterialMapper.delete(Wrappers.<WmNewsMaterial>lambdaQuery()
                .eq(WmNewsMaterial::getNewsId, id));

        //删除文章
        deleteByid(id);
    }

    /**
     * 修改或新增
     * @param wmNews
     * @param images
     */
    private void extracted(WmNews wmNews, List<String> images) {
        //补充信息
        wmNews.setSubmitedTime(new Date());
        wmNews.setUserId(WmThreadLocalUtil.getUser().getId());
        //获取封面图片集合
        String[] potoos = wmNews.getImages().split(",");
        ArrayList arrayList = new ArrayList<String>(Arrays.asList(potoos));
        //判断是否为草稿
        if (wmNews.getStatus() == 0) {
            if (wmNews.getId() == null) {
                wmNews.setCreatedTime(new Date());
                //草稿直接提交
                save(wmNews);
            } else {
                updateById(wmNews);
            }

        } else if (wmNews.getStatus() == 1) {
            if (wmNews.getId() == null) {
                wmNews.setCreatedTime(new Date());
                //添加文章，素材中间表内容
                //获取文章图片地址集合
                saveNewsMaterial(wmNews, (short) 0, images);
                //获取images，图片地址集合
                saveNewsMaterial(wmNews, (short) 0, arrayList);
                save(wmNews);

            } else {
                //先删除，添加文章，素材中间表内容
                wmNewsMaterialMapper.delete(Wrappers.<WmNewsMaterial>lambdaQuery()
                        .eq(WmNewsMaterial::getNewsId, wmNews.getId()));

                //添加信息
                //获取文章图片地址集合
                saveNewsMaterial(wmNews, (short) 0, images);
                //获取images，图片地址集合
                saveNewsMaterial(wmNews, (short) 0, arrayList);
                updateById(wmNews);

            }
        }
    }


    //图片封装方法
    public WmNews Potosfengzhuang(WmNews wmNews,List listimages) {
        int size = listimages.size();
        if (size>=1 && size<3) {
            //单图模式
            wmNews.setType((short) 1);
             listimages = (List) listimages.stream().limit(1).collect(Collectors.toList());
        } else if (size >= 3) {
            wmNews.setType((short) 3);
            listimages = (List) listimages.stream().limit(1).collect(Collectors.toList());
        } else {
            wmNews.setType((short) 0);
        }

        wmNews.setImages(StringUtils.join(listimages,","));
        return wmNews;


    }


    //图片抽取
    public List<String> Potocq(String content) {
        ArrayList<String> list = new ArrayList<>();
        List<Map> maps = JSON.parseArray(content, Map.class);
        for (Map map : maps) {
            if (map.get("type").equals("image")) {
                list.add((String) map.get("value"));

            }
        }

        return list;
    }




    //添加第三张表资料
    private void saveNewsMaterial(WmNews wmNews, short type, List<String> images) {
        //判断imageList是否有值
        if (images!=null &&images.size()>0) {
            //根据imag url集合获取image对应的id
            List<WmMaterial> wmMaterials = wmMaterialMapper.selectList(Wrappers.<WmMaterial>lambdaQuery().in(WmMaterial::getUrl, images));
            //获取对应id集合
            List<Integer> collect = wmMaterials.stream().map(WmMaterial::getId).collect(Collectors.toList());
            //添加数据
            wmNewsMaterialMapper.saveRelations(collect, wmNews.getId(), type);
        }
    }


}
