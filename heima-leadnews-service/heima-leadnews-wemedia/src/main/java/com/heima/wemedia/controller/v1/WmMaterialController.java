package com.heima.wemedia.controller.v1;

import com.heima.file.service.FileStorageService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.wemedia.pojos.WmMaterial;
import com.heima.model.common.wemedia.pojos.WmMaterialDto;
import com.heima.wemedia.service.WmMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/material")
public class WmMaterialController {

    @Autowired
    WmMaterialService wmMaterialService;

    @Autowired
    FileStorageService fileStorageService;

    /**
     * 添加图片素材
     * @param multipartFile
     * @return
     */
    @PostMapping("/upload_picture")
    public ResponseResult uploadPicture(MultipartFile multipartFile){
        return wmMaterialService.uploadPicture(multipartFile);
    }

    /**
     * 查询图片素材
     * @param dto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult findList(@RequestBody WmMaterialDto dto){

        return wmMaterialService.findList(dto);
    }

    /**
     * 收藏素材
     * @param id
     * @return
     */
    @GetMapping("/collect/{id}")
    public ResponseResult Collect(@PathVariable Long id) {
        WmMaterial wmMaterial = wmMaterialService.getById(id);
        Short collection = wmMaterial.getIsCollection();
        if (collection == 1) {
            wmMaterial.setIsCollection((short) 0);
        } else {
            wmMaterial.setIsCollection((short) 1);
        }

        //提交
        wmMaterialService.updateById(wmMaterial);
        return ResponseResult.okResult(wmMaterial);
    }


    /**
     * 根据id删除图片
     * @param id
     * @return
     */
    @GetMapping("/del_picture/{id}")
    public ResponseResult DeletewmM(@PathVariable Integer id) {
        //根据id查询图片
        WmMaterial wmMaterial = wmMaterialService.getById(id);
        //查询图片地址
        String url = wmMaterial.getUrl();
        //删除图片素材
        wmMaterialService.removeById(id);
        //删除minio图片
        fileStorageService.delete(url);
        return ResponseResult.okResult(wmMaterial);

    }

}
