package com.heima.wemedia.service.impl;

import com.heima.apis.schedule.IScheduleClient;
import com.heima.model.common.enums.TaskTypeEnum;
import com.heima.model.common.schedule.dtos.Task;
import com.heima.model.common.wemedia.pojos.WmNews;

import com.heima.utils.common.ProtostuffUtil;
import com.heima.wemedia.service.WmNewsTaskService;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
@Slf4j
public class WmNewsTaskServiceImpl  implements WmNewsTaskService {


    @Autowired
    private IScheduleClient scheduleClient;

    /**
     * 添加任务到延迟队列中
     * @param id          文章的id，供文章自动审核使用
     * @param publishTime 发布的时间  可以做为任务的执行时间
     */
    @Override
    @Async
    public void addNewsToTask(Integer id, Date publishTime) {

        log.info("添加任务到延迟服务中----begin");

        Task task=new Task();
        task.setExecuteTime(publishTime.getTime());
        task.setTaskType(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType());
        task.setPriority(TaskTypeEnum.NEWS_SCAN_TIME.getPriority());

        WmNews wmNews=new WmNews();
        wmNews.setId(id);

        task.setParameters(ProtostuffUtil.serialize(wmNews));

        scheduleClient.addTask(task);

        log.info("添加任务到延迟服务中----end");

    }

    @Autowired
    private WmNewsAutoScanServiceImpl wmNewsAutoScanService;


    /**
     * 消费list任务
     */
    @Scheduled(fixedRate = 50000)//设置为50s，要不时间太短
    @Override
    @SneakyThrows
    public void scanNewsByTask() {

        log.info("文章审核---消费任务执行---begin---");

        //1.拉取任务
        Task task = scheduleClient.poll(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType(), TaskTypeEnum.NEWS_SCAN_TIME.getPriority());
        if(task!=null && task.getParameters().length>0){
            byte[] bytes = task.getParameters();
            //反序列化
            WmNews wmNews = ProtostuffUtil.deserialize(bytes, WmNews.class);

            //2.调用自动审核业务
            Integer newsId=wmNews.getId();
            wmNewsAutoScanService.autoScanWmNews((int) newsId.longValue());

        }
        log.info("文章审核---消费任务执行---end---");
    }

}