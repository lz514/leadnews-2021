package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;

import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.wemedia.dto.WmNewsDto;
import com.heima.model.common.wemedia.dto.WmNewsPageReqDto;
import com.heima.model.common.wemedia.pojos.WmNews;
import com.heima.wemedia.service.WmNewsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;

@RestController
@RequestMapping("/api/v1/news")
public class WmNewsController {

    @Autowired
    WmNewsService wmNewsService;

    @PostMapping("/list")
    public ResponseResult findAll(@RequestBody WmNewsPageReqDto dto){
        return  wmNewsService.findAll(dto);
    }


    /**
     * 添加或修改文章
     * @param dto
     * @return
     */
    @PostMapping("/submit")
    public ResponseResult submitNews(@RequestBody WmNewsDto dto){
        return  wmNewsService.submitNews(dto);
    }

    /**
     * 查看文章详情
     * @param id
     * @return
     */
    @GetMapping("/one/{id}")
    public ResponseResult selectByid(@PathVariable Long id) {
        WmNews wn = wmNewsService.getById(id);
        WmNewsDto wmNewsDto = new WmNewsDto();
        BeanUtils.copyProperties(wn, wmNewsDto);
        String[] split = wn.getImages().split(",");
        ArrayList<String> list = new ArrayList<>(Arrays.asList(split));
        wmNewsDto.setImages(list);
        return ResponseResult.okResult(wmNewsDto);
    }

    /**
     * 文章上下架
     * @param wmNews
     * @return
     */
    @PostMapping("/down_or_up")
    public ResponseResult updateByid(@RequestBody WmNews wmNews) {
        wmNewsService.updateById(wmNews);
        return ResponseResult.okResult(wmNews);
    }


    /**
     * 文章删除
     * @param id
     * @return
     */
    @GetMapping("/del_news/{id}")
    public ResponseResult deleteByid(@PathVariable Long id) {
        wmNewsService.deleteByid(id);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}