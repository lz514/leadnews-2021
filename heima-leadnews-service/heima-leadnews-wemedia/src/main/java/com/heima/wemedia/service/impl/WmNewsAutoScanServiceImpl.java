package com.heima.wemedia.service.impl;


import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.injector.methods.UpdateById;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.apis.article.IArticleClient;
import com.heima.common.aliyun.GreenImageScan;
import com.heima.common.aliyun.GreenTextScan;
import com.heima.common.tess4j.Tess4jClient;
import com.heima.file.service.FileStorageService;
import com.heima.model.common.article.dtos.ArticleDto;
import com.heima.model.common.article.pojos.WmSensitive;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.wemedia.pojos.WmChannel;
import com.heima.model.common.wemedia.pojos.WmNews;
import com.heima.model.common.wemedia.pojos.WmUser;
import com.heima.utils.common.SensitiveWordUtil;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmSensitiveMapper;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.WmNewsAutoScanService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
@Transactional
public class WmNewsAutoScanServiceImpl implements WmNewsAutoScanService {

    //文章查询mapper
    @Autowired
    private WmNewsMapper wmNewsMapper;

    /**
     * 自媒体文章审核
     * @param id  自媒体文章id
     */
    @Override
    public void autoScanWmNews(Integer id) {
        //1.验证参数，查询数据
        WmNews wmNews = wmNewsMapper.selectById(id);
        if (wmNews==null) {
            throw new RuntimeException("WmNewsAutoScanServiceImpl-文章不存在");
        }

        //当状态为1，则是提交待审核
        if (wmNews.getStatus().equals(WmNews.Status.SUBMIT.getCode())) {
            //抽取内容提取文本内容，和图片
            Map<String,Object> textAndImages = handleTextAndImages(wmNews);
            //1.5自定义敏感词过滤
            boolean isSensitive = handleSensitiveScan((String) textAndImages.get("content"), wmNews);
            if(!isSensitive) return;
            //2.审核文本内容，阿里云接口
            boolean isTextScan = handleTextScan((String) textAndImages.get("content"),wmNews);
            if(!isTextScan)return;
            //3.审核图片，阿里云接口
            boolean isImageScan =  handleImageScan((List<String>) textAndImages.get("images"),wmNews);
            if(!isImageScan)return;
            //4.审核成功，保存app端的相关文章数据

            ResponseResult responseResult = saveAppArticle(wmNews);
            if(!responseResult.getCode().equals(200)){
                throw new RuntimeException("WmNewsAutoScanServiceImpl-文章审核，保存app端相关文章数据失败");
            }
            //5.回填article_id
            wmNews.setArticleId((Long) responseResult.getData());
            updateWmNews(wmNews,(short) 9,"审核成功");
        }

    }

    @Autowired
    private WmSensitiveMapper wmSensitiveMapper;

    /**
     * 自定义敏感词过滤
     * @param content
     * @param wmNews
     * @return
     */
    private boolean handleSensitiveScan(String content, WmNews wmNews) {
        boolean flag = true;
        //Daf算法
        //获取所有敏感词
        List<WmSensitive> wmSensitives = wmSensitiveMapper.selectList(null);
        List<String> stringStream = wmSensitives.stream().map(WmSensitive::getSensitives).collect(Collectors.toList());
        //初始化敏感词库
        SensitiveWordUtil.initMap(stringStream);

        //查看文章是否包含敏感词
        Map<String, Integer> stringIntegerMap = SensitiveWordUtil.matchWords(content);
        if (stringIntegerMap.size()>0) {
            updateWmNews(wmNews,(short) 2,"当前文章中存在违规内容");
            flag = false;

        }
        return flag;


    }


    @Autowired
    private IArticleClient articleClient;

    @Autowired
    private WmChannelMapper wmChannelMapper;

    @Autowired
    private WmUserMapper wmUserMapper;


    //1.抽取文本内容，图片为Map<string,object>集合
    private Map<String, Object> handleTextAndImages(WmNews wmNews) {
        //存储文本内容
        StringBuilder stringBuilder = new StringBuilder();
        //存储照片地址
        ArrayList<String> images = new ArrayList<>();

        //1.从文章中提取文本内容和照片地址
        if (StringUtils.isNotBlank( wmNews.getContent())) {//不为空进行提取
            //将json字符串转成对象或map集合
            List<Map> maps = JSONArray.parseArray(wmNews.getContent(), Map.class);
            //将map集合进行遍历，并将对应的数据存放到集合中
            for (Map map : maps) {
                //判断文章
                if (map.get("type").equals("text")) {
                    stringBuilder.append(map.get("value"));
                }

                //判断图片
                if (map.get("type").equals("image")) {
                    images.add(map.get("value")+"");
                }
            }
            //再提取封面图片到图片地址集合中
            if (StringUtils.isBlank(wmNews.getImages())) {
                String[] split = wmNews.getImages().split(",");
                images.addAll(Arrays.asList(split));
            }
        }
        //将数据保存到map集合中并返回
        HashMap<String, Object> map = new HashMap<>();
        map.put("content",stringBuilder.toString());
        map.put("images",images);
        return map;
    }

    @Autowired
    private GreenTextScan greenTextScan;

    //2.审核文本内容方法
    private boolean handleTextScan(String content, WmNews wmNews) {
        boolean flag = true;
        //判断标题和文章是否为空
        if ((wmNews.getTitle()+content).length()==0) {
            return flag;
        }

        try {
            Map map = greenTextScan.greeTextScan(wmNews.getTitle() + content);
            if (map.get("suggestion").equals("block")) {
                flag = false;
                updateWmNews(wmNews, (short) 2, "当前文章中存在违规内容");
            }

            //不确定信息  需要人工审核
            if(map.get("suggestion").equals("review")){
                flag = false;
                updateWmNews(wmNews, (short) 3, "当前文章中存在不确定内容");
            }
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }

        return flag;
    }

    /**
     * 修改文章
     * @param wmNews
     * @param i
     * @param co
     */
    private void updateWmNews(WmNews wmNews, short i, String co) {
        wmNews.setStatus(i);
        wmNews.setReason(co);
        wmNewsMapper.updateById(wmNews);
    }

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private GreenImageScan greenImageScan;

    @Autowired
    private Tess4jClient tess4jClient;

    //3.审核图片内容
    private boolean handleImageScan(List<String> images, WmNews wmNews) {
        boolean flag = true;

        if(images == null || images.size() == 0){
            return flag;
        }

        //图片地址重复过滤
        images=images.stream().distinct().collect(Collectors.toList());
        //将下载的图片存放到集合中
        List<byte[]> imageList = new ArrayList<>();
        try {
            //图片文字审核
            for (String image : images) {
                //从minio中下载图片
                byte[] bytes = fileStorageService.downLoadFile(image);
                //图片识别文字审核
                //从byte[]转换为butteredImage
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
                BufferedImage read = ImageIO.read(byteArrayInputStream);
                //识别图片文字
                String result = tess4jClient.doOCR(read);
                //审核是否包含自管理的敏感词
                boolean isSensitive = handleSensitiveScan(result, wmNews);
                if(!isSensitive){ //为falls则返回
                    return isSensitive;
                }

                imageList.add(bytes);

            }
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }


        try {
            //审核图片
            Map map = greenImageScan.imageScan(imageList);
            if (map.get("suggestion").equals("block")) {
                flag = false;
                updateWmNews(wmNews, (short) 2, "当前文章中存在违规内容");
            }

            if (map.get("suggestion").equals("review")) {
                flag = false;
                updateWmNews(wmNews, (short) 3, "当前文章中存在不确定内容");
            }

        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }
        return flag;
    }

    //4.审核成功保存app相关文章数据，并异步发送fein请求
    @Async
    public ResponseResult saveAppArticle(WmNews wmNews) {

        ArticleDto articleDto = new ArticleDto();
        //属性拷贝
        BeanUtils.copyProperties(wmNews,articleDto);
        //文章布局
        articleDto.setLabels(wmNews.getType()+"");
        //频道名称
        WmChannel wmChannel = wmChannelMapper.selectById(wmNews.getChannelId());
        if (wmChannel!=null) {
            articleDto.setChannelName(wmChannel.getName());
        }
        //作者
        articleDto.setAuthorId(wmNews.getUserId().longValue());
        WmUser wmUser = wmUserMapper.selectById(wmNews.getUserId());
        if (wmUser!=null) {
            articleDto.setAuthorName(wmUser.getName());
        }
        //设置文章id
        if (wmNews.getArticleId()!=null) {
            articleDto.setId(wmNews.getArticleId());
        }

        //异步提交
        ResponseResult responseResult = articleClient.saveArticle(articleDto);
        return responseResult;

    }


}
