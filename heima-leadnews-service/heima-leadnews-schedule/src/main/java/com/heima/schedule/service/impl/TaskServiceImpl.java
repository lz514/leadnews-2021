package com.heima.schedule.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.common.constants.ScheduleConstants;
import com.heima.common.redis.RedisCacheService;
import com.heima.model.common.schedule.dtos.Task;
import com.heima.model.common.schedule.pojos.Taskinfo;
import com.heima.model.common.schedule.pojos.TaskinfoLogs;
import com.heima.schedule.mapper.TaskinfoLogsMapper;
import com.heima.schedule.mapper.TaskinfoMapper;
import com.heima.schedule.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

@Slf4j
@Service
@Transactional
public class TaskServiceImpl implements TaskService {

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    /**
     * 添加延迟任务
     * @param task   任务对象
     * @return
     */
    @Override
    public long addTask(Task task) {


        //1.添加任务到数据库中
        boolean success= addTaskToDb(task);
        if (success) {
            //2.添加任务到redis
            addTaskToCache(task);
        }
        return task.getTaskId();


    }

    @Autowired
    private RedisCacheService redisCacheService;

    /**
     * 添加缓存到redis
     * @param task
     */
    @Async
     void addTaskToCache(Task task) {

        //自定义key
     String key=   task.getTaskType() + "_" + task.getPriority();

     //判断时间，小于当前时间存放在list集合中
        if (task.getExecuteTime()<=System.currentTimeMillis()) {//获取当前时间的毫秒值
            redisCacheService.lLeftPush(ScheduleConstants.TOPIC + key, JSON.toJSONString(task));
        }
        //大于当前时间，小于等于当前时间加5分钟存放在zset集合中
        //获取预设时间=当前时间+5分钟
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.MINUTE,5);
        if (task.getExecuteTime()>=System.currentTimeMillis()&&task.getExecuteTime()<=instance.getTimeInMillis()) {
            redisCacheService.zAdd(ScheduleConstants.FUTURE + key, JSON.toJSONString(task),task.getExecuteTime());
        }


    }


    @Autowired
    private TaskinfoMapper taskinfoMapper;

    @Autowired
    private TaskinfoLogsMapper taskinfoLogsMapper;

    /**
     * 添加任务到数据库中
     * @param task
     * @return
     */
    private boolean addTaskToDb(Task task) {
        boolean flag = false;

        try {
            Taskinfo taskinfo = new Taskinfo();
            BeanUtils.copyProperties(task,taskinfo);
            taskinfo.setExecuteTime(new Date(task.getExecuteTime()));
            taskinfoMapper.insert(taskinfo);

            //taskID
            task.setTaskId(taskinfo.getTaskId());

            //保存任务数据日志
            TaskinfoLogs taskinfoLogs = new TaskinfoLogs();
            BeanUtils.copyProperties(taskinfo,taskinfoLogs);
            taskinfoLogs.setVersion(1);
            taskinfoLogs.setStatus(ScheduleConstants.SCHEDULED);
            taskinfoLogsMapper.insert(taskinfoLogs);

            return true;
        } catch (BeansException e) {
            e.printStackTrace();
        }

        return flag;
    }


    /**
     * 取消任务
     * @param taskId        任务id
     * @return
     */
    @Override
    public boolean cancelTask(long taskId) {
        boolean flag = false;
        //删除任务，更新日志
        Task task = updateDb(taskId,ScheduleConstants.EXECUTED);

        //删除redis的数据
        if(task != null){
            removeTaskFromCache(task);
            flag = true;
        }
        return false;
    }

    /**
     * 按照类型和优先级拉取任务
     * @return
     */
    @Override
    public Task poll(int type, int priority) {
        Future<Task> poll_task_exception = threadPoolTaskExecutor.submit(new Callable<Task>() {
            @Override
            public Task call() throws Exception {
                Task task = null;
                try {
                    String key = type + "_" + priority;
                    String task_json = redisCacheService.lRightPop(ScheduleConstants.TOPIC + key);
                    if (StringUtils.isNotBlank(task_json)) {
                        task = JSON.parseObject(task_json, Task.class);
                        //更新数据库信息
                        updateDb(task.getTaskId(), ScheduleConstants.EXECUTED);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("poll task exception");
                }

                return task;
            }
        });

        try {
            Task task = poll_task_exception.get(5, TimeUnit.SECONDS);
            return task;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 删除redis中的任务数据
     * @param task
     */
    @Async
     void removeTaskFromCache(Task task) {

        String key = task.getTaskType()+"_"+task.getPriority();

        if(task.getExecuteTime()<=System.currentTimeMillis()){
            redisCacheService.lRemove(ScheduleConstants.TOPIC+key,0,JSON.toJSONString(task));
        }else {
            redisCacheService.zRemove(ScheduleConstants.FUTURE+key, JSON.toJSONString(task));
        }
    }



    /**
     * 定时任务，将zset数据定时从list
     */
    @Scheduled(cron = "0 */1 * * * ?")
    public void  refresh() {
        threadPoolTaskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                //加redis setnx锁
                String token = redisCacheService.tryLock("FUTURE_TASK_SYNC", 1000 * 30);
                if (StringUtils.isNotBlank(token)) {
                    System.out.println(System.currentTimeMillis() / 1000 + "执行了定时任务");

                    //获取所有未来数据的key
                    Set<String> scan = redisCacheService.scan(ScheduleConstants.FUTURE + "*");
                    System.out.println(new Date());
                    System.out.println(new Date().getTime());
                    System.out.println(System.currentTimeMillis());
                    for (String futureKey: scan) {
                        //获取改组key下当前需要消费的任务数据
                        Set<String> tasks = redisCacheService.zRangeByScore(futureKey, 0, System.currentTimeMillis());
                        System.out.println(tasks);
                        if (tasks.isEmpty()) {
                            //获取topckey
                            String topicKey= ScheduleConstants.TOPIC + futureKey.split(ScheduleConstants.FUTURE)[1];
                            //将这些任务数据添加到消费者队列中
                            redisCacheService.refreshWithPipeline(futureKey, topicKey, tasks);
                        }
                    }
                }

            }
        });


    }


    @Scheduled(cron = "0 */5 * * * ?")
    @PostConstruct
    public void reloadData() {
        threadPoolTaskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                //删除zset和list类型的所有数据
                clearCache();

                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE,5);

                //查看小于未来5分钟的所有数据任务
                List<Taskinfo> taskinfos = taskinfoMapper.selectList(Wrappers.<Taskinfo>lambdaQuery().lt(Taskinfo::getExecuteTime, calendar.getTime()));
                if (taskinfos!=null && taskinfos.size()>0) {
                    for (Taskinfo taskinfo : taskinfos) {
                        Task task = new Task();
                        BeanUtils.copyProperties(taskinfo,task);
                        task.setExecuteTime(taskinfo.getExecuteTime().getTime());
                        //将数据进行添加
                        addTaskToCache(task);
                    }
                }
            }
        });




    }


    @Async
    public void clearCache() {
        // 删除缓存中未来数据集合和当前消费者队列的所有key
        Set<String> futurekeys = redisCacheService.scan(ScheduleConstants.FUTURE + "*");
        Set<String> topickeys = redisCacheService.scan(ScheduleConstants.TOPIC + "*");
        redisCacheService.delete(futurekeys);
        redisCacheService.delete(topickeys);
    }


    /**
     * 删除任务，更新任务日志状态
     * @param taskId
     * @param status
     * @return
     */
    private Task updateDb(long taskId, int status) {
        Task task = null;
        try {
            //删除任务
            taskinfoMapper.deleteById(taskId);

            TaskinfoLogs taskinfoLogs = taskinfoLogsMapper.selectById(taskId);
            taskinfoLogs.setStatus(status);
            taskinfoLogsMapper.updateById(taskinfoLogs);

            task = new Task();
            BeanUtils.copyProperties(taskinfoLogs,task);
            task.setExecuteTime(taskinfoLogs.getExecuteTime().getTime());
        }catch (Exception e){
            log.error("task cancel exception taskid={}",taskId);
        }

        return task;

    }

}
