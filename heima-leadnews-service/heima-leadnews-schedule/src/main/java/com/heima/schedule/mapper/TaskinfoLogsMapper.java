package com.heima.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.schedule.pojos.TaskinfoLogs;

public interface TaskinfoLogsMapper extends BaseMapper<TaskinfoLogs> {
}
