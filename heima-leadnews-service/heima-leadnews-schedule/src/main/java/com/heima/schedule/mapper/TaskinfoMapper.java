package com.heima.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.schedule.pojos.Taskinfo;

public interface TaskinfoMapper extends BaseMapper<Taskinfo> {
}
